package INF102.lab5.graph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;
    private ArrayList<HashSet<V>> completedPaths;

    public GraphSearch(IGraph<V> graph) {
        this.completedPaths = new ArrayList<>();
        this.graph = graph;
    }

    public boolean connected(V u, V v) {
    for (HashSet<V> pathh : completedPaths) {
        if (pathh.contains(u) && pathh.contains(v)) {
            return true;
        }
    }
    Queue<V> frontier = new LinkedList<>();
    Set<V> checkedNodes = new HashSet<>();
    HashSet<V> path = new HashSet<>();
    if (graph.getNeighbourhood(u) == null || graph.getNeighbourhood(v) == null) {
        return false;
    }
    frontier.addAll(graph.getNeighbourhood(u));
    // While there are neighbours to the node, go to next node and check if goal.
    while (true) {
        if (frontier.isEmpty()) {
            return false;
        }
        // Take first node out and inspect
        V current = frontier.poll();
        Set<V> currentNeighbours = graph.getNeighbourhood(current);
        if (current.equals(v)) {
            path.add(current);
            completedPaths.add(path);
            return true;
        }
        else if (!currentNeighbours.isEmpty()) {
            checkedNodes.add(current);
            path.add(current);
            for (V node : currentNeighbours) {
                if (!checkedNodes.contains(node)) {
                    frontier.add(node);
                }
            }
        }
        else {
            path.remove(current);
        }
    }        
}
}
