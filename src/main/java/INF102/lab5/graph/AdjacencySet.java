package INF102.lab5.graph;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {

    private Set<V> nodes;
    private Map<V, Set<V>> nodeToNode;

    public AdjacencySet() {
        nodes = new HashSet<>();
        nodeToNode = new HashMap<>();
    }

    @Override
    public int size() {
        return nodes.size();
    }

    @Override
    public Set<V> getNodes() {
        return Collections.unmodifiableSet(nodes);
    }

    @Override
    public void addNode(V node) {
        nodes.add(node);
        if (!nodeToNode.containsKey(node)) {
            nodeToNode.put(node, new HashSet<>());
        }
        
    }

    @Override
    public void removeNode(V node) {
        if (nodeToNode.get(node) != null) {
            Set<V> nodes = new HashSet<>(getNeighbourhood(node));
            for (V n : nodes) {
                removeEdge(n, node);
            }
            nodeToNode.remove(node);
        }
        nodes.remove(node);       
    }

    @Override
    public void addEdge(V u, V v) {
        if (!hasNode(v) || !hasNode(u)) {
            throw new IllegalArgumentException("You can't add edge to a node that doesn't exist");
        }
        if (nodeToNode.get(u) != null) {
            Set<V> set = nodeToNode.get(u);
            set.add(v);
            nodeToNode.replace(u, set);         
        } 
        else {
            Set<V> set = new HashSet<>();
            set.add(v);
            nodeToNode.put(u, set);
        }
        if (nodeToNode.get(v) != null){
            Set<V> set2 = nodeToNode.get(v);
            set2.add(u);
            nodeToNode.replace(v, set2);
        }
        else  {
            Set<V> set2 = new HashSet<>();
            set2.add(u);
            nodeToNode.put(v, set2);
        }   
    }

    @Override
    public void removeEdge(V u, V v) {
        if (nodeToNode.get(u) != null && nodeToNode.get(v) != null) {
            Set<V> set = nodeToNode.get(u);
            set.remove(v);
            nodeToNode.replace(u, set);
            Set<V> set2 = nodeToNode.get(u);
            set2.remove(u);
            nodeToNode.replace(v, set2);
        }
    }

    @Override
    public boolean hasNode(V node) {
        return nodes.contains(node);
    }

    @Override
    public boolean hasEdge(V u, V v) {
        return nodeToNode.get(u).contains(v);
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        return Collections.unmodifiableSet(nodeToNode.get(node));
    }

    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();
        for (V node : nodeToNode.keySet()) {
            Set<V> nodeList = nodeToNode.get(node);

            build.append(node);
            build.append(" --> ");
            build.append(nodeList);
            build.append("\n");
        }
        return build.toString();
    }

}
